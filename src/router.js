"use strict";

import express from "express";
import cats from "../data/cats";
import users from "../data/users";
import { v4 as uuidv4 } from 'uuid';
import verifyToken from "./middleware/jwtVerify"

const router = express.Router();
let jwt = require("jsonwebtoken");
let privateKey = "shhhhh";

router.get("/", (req, res) => {
  return res.status(200).send("Hello World");
});

router.get("/cats", verifyToken, (req, res) => {
  return res.status(200).send(cats);
});

router.post("/cats", (req, res) => {
  let newCat = {
    id: uuidv4(),
    name: req.body.name,
    breed: req.body.breed,
  };
  cats.push(newCat);
  return res.status(201).send(cats);
});

router.post("/register", (req, res) => {
  let newUser = {
    email: req.body.email,
    password: req.body.password,
  };
  users.push(newUser);
  let token = jwt.sign(newUser, privateKey);
  return res.status(201).send(token);
});

router.get("*", (req, res, next) => {
  let err = new Error("typed wring URL")
  next(err)
  
})
export default router;
